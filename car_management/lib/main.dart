import 'package:car_management/controllers/auth_widget.dart';
import 'package:car_management/controllers/auth_widget_builder.dart';
import 'package:car_management/services/firebase_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';

void main() {
  initializeDateFormatting().then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<FirebaseAuthService>(
          create: (_) => FirebaseAuthService(),
        ),
      ],
      child: AuthWidgetBuilder(builder: (context, userSnapshot) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: AuthWidget(userSnapshot: userSnapshot),
        );
      }),
    );
  }
}
