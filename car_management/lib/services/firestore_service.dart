import 'dart:io';
import 'package:car_management/models/panel_reference.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class FirestoreService {
  FirestoreService({@required this.uid}) : assert(uid != null);
  final String uid;

  /// Upload an image from file
  Future<String> uploadImage(
          {@required String uid,
          @required File file,
          @required String imgName,
          @required String collection}) async =>
      await upload(
        uid: uid,
        file: file,
        path: '$collection/$uid/${DateTime.now()}-$imgName',
        contentType: 'image/${file.path.split('.').last}',
      );

  /// Generic file upload for any [path] and [contentType]
  Future<String> upload({
    @required String uid,
    @required File file,
    @required String path,
    @required String contentType,
  }) async {
    //getting data
    print('uploading to: $path');
    final storageReference = FirebaseStorage.instance.ref().child(path);
    final uploadTask = storageReference.putFile(
        file, StorageMetadata(contentType: contentType));
    final snapshot = await uploadTask.onComplete;
    if (snapshot.error != null) {
      print('upload error code: ${snapshot.error}');
      throw snapshot.error;
    }
    // Url used to download file/image
    final downloadUrl = await snapshot.ref.getDownloadURL();
    print('downloadUrl: $downloadUrl');
    return downloadUrl;
  }

  //Upload multiple images from list

  Future<List<PanelReference>> uploadMultipleImages(
      {@required String uid,
      @required List<dynamic> files,
      @required String collection}) async {
    if (files != null) {
      List<PanelReference> panelsReference = [];
      String downloadUrl;
      for (var i = 0; i < files.length; i++) {
        try {
          downloadUrl = await uploadImage(
              uid: uid,
              file: files[i].image,
              collection: collection,
              imgName: '${files[i].image.path.split('/').last}');
          if (downloadUrl == null) {
            return null;
          }
          panelsReference.add(new PanelReference(
              downloadUrl: downloadUrl, name: files[i].name));
        } catch (e) {
          print('ERROR UPLOADING MULTIPLE IMAGES: ' + e.toString());
        }
      }
      return panelsReference;
    } else {
      return null;
    }
  }
}
