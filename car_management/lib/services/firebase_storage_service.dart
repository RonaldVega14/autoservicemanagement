import 'dart:async';
import 'package:car_management/models/car_model.dart';
import 'package:car_management/models/manufacturer_model.dart';
import 'package:car_management/models/panel_model.dart';
import 'package:car_management/services/firestore_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:car_management/models/panel_reference.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FirebaseStorageService {
  FirebaseStorageService({@required this.uid}) : assert(uid != null);
  final String uid;
  final _db = Firestore.instance;
  // Sets the panel download url
  Future<void> setPanelReference({
    @required String imageUID,
    @required PanelReference panelReference,
    @required String collection,
  }) async {
    final path = collection + '/$imageUID';
    final reference = _db.document(path);
    await reference.setData(panelReference.toMap(), merge: true);
  }

  // Reads the current avatar download url
  Stream<PanelReference> panelReferenceStream({
    @required String imageUID,
    @required String collection,
  }) {
    final path = collection + '/$imageUID';
    final reference = _db.document(path);
    final snapshots = reference.snapshots();
    return snapshots.map((snapshot) => PanelReference.fromMap(snapshot.data));
  }

  //Set multiple panels info
  Future<void> setMultiplePanelReference({
    @required String docID,
    @required List<PanelReference> panelReference,
    @required String collection,
  }) async {
    final path = collection + '/$docID';
    final reference = _db.document(path);

    Map<String, dynamic> data = {
      'damagedPanels': panelReference.map((panel) => panel.toMap()).toList()
    };
    await reference.setData(data, merge: true);
  }

  Future<String> addData(Car data, String collection) async {
    String docID;
    if (data != null) {
      try {
        DocumentReference docRef =
            await _db.collection(collection).add(data.toMap());
        docID = docRef.documentID;
      } catch (e) {
        print('ERROR IN FIREBASE SERVICE FUNCTION \'addData\' ');
      }
    }
    return docID;
  }

  Future<bool> uploadCarData(
      List<Panel> panelsInfo, Car carInfo, BuildContext context) async {
    //Obtainin database provider
    final firestore = Provider.of<FirestoreService>(context, listen: false);
    try {
      String docID = await addData(carInfo, 'cars');
      Map<String, dynamic> idData = {
        'id': docID,
      };
      final reference = _db.document('cars/$docID');
      reference.setData(idData, merge: true);
      List<PanelReference> panelsReference = [];
      //Uploading images to cloud Firestore
      panelsReference = await firestore.uploadMultipleImages(
          uid: docID, files: panelsInfo, collection: 'cars');
      //Deleting files
      panelsInfo.remove(panelsInfo.length);
      //Uploading images to firebase database
      setMultiplePanelReference(
          docID: docID, panelReference: panelsReference, collection: 'cars');
      return true;
    } catch (e) {
      print('ERROR UPLOADING CAR DATA:' + e.toString());
    }
    return false;
  }

  Future<QuerySnapshot> getCollectionDocuments({@required collection}) async {
    return _db.collection(collection).getDocuments();
  }

  Stream<QuerySnapshot> getCollectionStream({@required collection}) {
    return _db.collection(collection).snapshots();
  }

  Stream<QuerySnapshot> getSubCollectionStream(
      {@required collection, @required subCollection, @required id}) {
    return _db
        .collection(collection)
        .document(id)
        .collection(subCollection)
        .snapshots();
  }

  Stream<List<Car>> getCars() {
    var snapshots = getCollectionStream(collection: 'cars');

    return snapshots.map(
        (list) => list.documents.map((doc) => Car.fromFirestore(doc)).toList());
  }

  Stream<List<Manufacturer>> getManufacturers() {
    var snapshots = getCollectionStream(collection: 'manufacturers');

    return snapshots.map((list) =>
        list.documents.map((doc) => Manufacturer.fromFirestore(doc)).toList());
  }
}
