import 'dart:io';

import 'package:car_management/changeNotifiers/addCar_notifier.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';

class CustomSelectImageWidget extends StatefulWidget {
  final image, index;
  final AddCarNotifier addCarNotifier;

  const CustomSelectImageWidget({
    Key key,
    @required this.image,
    @required this.index,
    @required this.addCarNotifier,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => CustomSelectImageWidgetState();
}

class CustomSelectImageWidgetState extends State<CustomSelectImageWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  //getters
  File get _image => widget.image;
  int get _index => widget.index;
  AddCarNotifier get _addCarNotifier => widget.addCarNotifier;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 350));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.pop(context),
        child: Center(
            child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation,
            child: buildGaleriaCamara(MediaQuery.of(context).size.height * .35,
                MediaQuery.of(context).size.width * .65, _image, _index),
          ),
        )));
  }

  Future<void> _openMedia(int option, File image, int index) async {
    File _image;
    try {
      switch (option) {
        case 1:
          _image = await ImagePicker.pickImage(source: ImageSource.camera);

          break;
        case 2:
          _image = await ImagePicker.pickImage(source: ImageSource.gallery);

          break;
        default:
      }
      if (_image != null) {
        image = _image;
        _addCarNotifier.addPanelImage(index, image);
      }
      Navigator.of(context).pop();
      return _image;
    } catch (e) {
      print('Error al cargar imagen: ' + e.toString());
    }
  }

  Widget buildGaleriaCamara(
      double height, double width, File image, int index) {
    return Center(
        child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Container(
              width: width,
              height: height,
              padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 13.0),
              margin: const EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 12.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(24.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(spreadRadius: 2.0, color: Colors.white)
                  ]),
              child: ListView(
                reverse: false,
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  SizedBox(
                    height: height * 0.08,
                  ),
                  Text(
                    'UPLOAD IMAGE FROM',
                    style: GoogleFonts.lato(
                        fontWeight: FontWeight.w800,
                        fontSize: 18.0,
                        textStyle: TextStyle(color: Color(0xff516c8d))),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: height * 0.08),
                  Material(
                    elevation: 3.0,
                    borderRadius: BorderRadius.circular(16.0),
                    color: Color(0xff28385e),
                    child: Tooltip(
                      message: 'Upload image from gallery',
                      child: MaterialButton(
                          padding: EdgeInsets.fromLTRB(10.0, 3.0, 10.0, 3.0),
                          onPressed: () {
                            _openMedia(2, image, index);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child:
                                        Icon(Icons.image, color: Colors.white),
                                  )),
                              Flexible(
                                flex: 4,
                                fit: FlexFit.loose,
                                child: Text('GALLERY',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.lato(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15.0,
                                        textStyle:
                                            TextStyle(color: Colors.white))),
                              )
                            ],
                          )),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.1,
                  ),
                  Material(
                    elevation: 3.0,
                    borderRadius: BorderRadius.circular(16.0),
                    color: Color(0xff28385e),
                    child: Tooltip(
                      message: 'Upload image from camera',
                      child: MaterialButton(
                          padding: EdgeInsets.fromLTRB(10.0, 3.0, 10.0, 3.0),
                          onPressed: () {
                            _openMedia(1, image, index);
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Flexible(
                                  flex: 1,
                                  fit: FlexFit.tight,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Icon(Icons.camera_alt,
                                        color: Colors.white),
                                  )),
                              Flexible(
                                flex: 4,
                                fit: FlexFit.loose,
                                child: Text('CAMERA',
                                    textAlign: TextAlign.center,
                                    style: GoogleFonts.lato(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15.0,
                                        textStyle:
                                            TextStyle(color: Colors.white))),
                              )
                            ],
                          )),
                    ),
                  ),
                  InkWell(
                    onTap: () => Navigator.of(context).pop(),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 22.0),
                      child: Text('CANCEL',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.lato(
                              fontWeight: FontWeight.w600,
                              fontSize: 15.0,
                              textStyle: TextStyle(color: Color(0xff516c8d)))),
                    ),
                  )
                ],
              ),
            )));
  }
}

class CustomDialogWidget extends StatefulWidget {
  final String msg;
  final bool answer;
  const CustomDialogWidget({Key key, @required this.answer, @required this.msg})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => CustomDialogWidgetState();
}

class CustomDialogWidgetState extends State<CustomDialogWidget>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;
  String get _msg => widget.msg;
  bool get _answer => widget.answer;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 350));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Center(
          child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: buildContent(MediaQuery.of(context).size.height * .35,
              MediaQuery.of(context).size.width * .65),
        ),
      )),
    );
  }

  Widget buildContent(double height, double width) {
    return Center(
        child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Container(
                width: width,
                height: height,
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 13.0),
                margin: const EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 12.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(24.0),
                    boxShadow: <BoxShadow>[
                      BoxShadow(spreadRadius: 2.0, color: Colors.white)
                    ]),
                child: ListView(
                    reverse: false,
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      SizedBox(
                        height: height * 0.08,
                      ),
                      Container(
                        height: height * 0.3,
                        margin: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          shape: BoxShape.circle,
                        ),
                        child: _answer
                            ? Image.asset('assets/success.png')
                            : Image.asset('assets/error.png'),
                      ),
                      Container(
                        height: height * 0.5,
                        child: Center(
                          child: Text(
                            _msg,
                            textAlign: TextAlign.center,
                            style: GoogleFonts.lato(
                                fontWeight: FontWeight.w600,
                                fontSize: 25.0,
                                textStyle: TextStyle(color: Color(0xff516c8d))),
                          ),
                        ),
                      )
                    ]))));
  }
}
