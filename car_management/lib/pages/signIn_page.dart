import 'package:car_management/services/firebase_auth_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

String _email, _password;

class SignInPage extends StatelessWidget {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  //Email
  final emailField = Material(
      elevation: 4.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Colors.transparent,
      child: TextFormField(
        validator: (val) => !val.contains('@') ? 'Invalid Email' : null,
        onSaved: (val) => _email = val,
        style: GoogleFonts.lato(
            fontSize: 16.0, textStyle: TextStyle(color: Color(0xff516c8d))),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.person, color: Color(0xff516c8d)),
            fillColor: Colors.white,
            filled: true,
            labelText: 'Email',
            contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
      ));
//Password
  final passwordField = Material(
      elevation: 4.0,
      borderRadius: BorderRadius.circular(12.0),
      color: Colors.transparent,
      child: TextFormField(
        validator: (val) => !(val.length > 6) ? 'Invalid Password' : null,
        onSaved: (val) => _password = val,
        obscureText: true,
        style: GoogleFonts.lato(
            fontSize: 16.0, textStyle: TextStyle(color: Color(0xff516c8d))),
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.vpn_key, color: Color(0xff516c8d)),
            fillColor: Colors.white,
            filled: true,
            labelText: 'Password',
            contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(12.0))),
      ));

//Login
  Widget loginButton(BuildContext context) => Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(12.0),
        color: Colors.white,
        child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width * 0.5,
            onPressed: () => _signIn(context),
            child: Text("LOGIN",
                textAlign: TextAlign.center,
                style: GoogleFonts.lato(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w600,
                    textStyle: TextStyle(color: Color(0xff516c8d))))),
      );

//Metodo para ingresar a la pagina.
  void _signIn(BuildContext context) async {
    final form = formKey.currentState;

    if (form.validate()) {
      form.save();

      try {
        final auth = Provider.of<FirebaseAuthService>(context, listen: false);
        final user = await auth.signIn(_email, _password);
        print('uid: ${user.uid}');
      } catch (e) {
        print(e);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        resizeToAvoidBottomInset: false,
        body: Stack(children: <Widget>[
          ClipPath(
            child: Container(color: Color(0xff516c8d)),
            clipper: GetClipper(),
          ),
          ClipPath(
            child: Container(color: Colors.white),
            clipper: GetInnerClipper(),
          ),
          Positioned(
              width: MediaQuery.of(context).size.width,
              top: MediaQuery.of(context).size.height * 0.10,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: Form(
                  key: formKey,
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.35,
                          child: Image.asset('assets/logo-letters.png',
                              scale: 0.3),
                        ),
                        emailField,
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.05),
                        passwordField,
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.10),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: loginButton(context),
                        ),
                        SizedBox(
                            height: MediaQuery.of(context).size.height * 0.10),
                      ],
                    ),
                  ),
                ),
              )),
        ]));
  }
}

class GetClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height * 0.05);
    path.lineTo(size.width * 0.8, size.height / 2);
    path.lineTo(0.0, size.height * 0.95);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class GetInnerClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height * 0.24);
    path.lineTo(size.width * 0.45, size.height / 2);
    path.lineTo(0.0, size.height * 0.76);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

/*
Smaller lines
class GetClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height * 0.15);
    path.lineTo(size.width * 0.7, size.height / 2);
    path.lineTo(0.0, size.height * 0.85);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}

class GetInnerClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height * 0.32);
    path.lineTo(size.width * 0.35, size.height / 2);
    path.lineTo(0.0, size.height * 0.68);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
 */
