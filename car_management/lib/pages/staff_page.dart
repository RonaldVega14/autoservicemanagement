import 'package:car_management/pages/profile_page.dart';
import 'package:car_management/pages/staffProfile_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class StaffPage extends StatelessWidget {
  final heroTag, optionName, imgPath;

  const StaffPage(
      {Key key,
      @required this.heroTag,
      @required this.optionName,
      @required this.imgPath})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    Widget _staffItem(String staffName, String staffPosition,
        String staffNumber, String imgPath) {
      return Center(
        child: Hero(
          tag: staffName + staffNumber,
          child: GestureDetector(
            onTap: (() => {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => StaffProfilePage(
                            heroTag: staffName + staffNumber,
                            imgPath: imgPath,
                            staffName: staffName,
                            staffNumber: staffNumber,
                            staffPosition: staffPosition,
                          )))
                }),
            child: Container(
              height: MediaQuery.of(context).size.height * 0.125,
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Flexible(
                    flex: 4,
                    fit: FlexFit.loose,
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Center(
                        child: Container(
                          height: (MediaQuery.of(context).size.height * 0.125) *
                              0.7,
                          width:
                              ((MediaQuery.of(context).size.width * 0.4) - 32) *
                                  0.7,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.transparent,
                            image: DecorationImage(
                                image: NetworkImage(imgPath),
                                fit: BoxFit.cover),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Flexible(
                      flex: 10,
                      fit: FlexFit.tight,
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 2.0),
                              child: Text(staffName,
                                  style: GoogleFonts.lato(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                      textStyle:
                                          TextStyle(color: Colors.black))),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 2.0),
                              child: Text(staffPosition,
                                  style: GoogleFonts.lato(
                                      fontSize: 14.0,
                                      textStyle:
                                          TextStyle(color: Colors.black54))),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 3.0),
                              child: Text(staffNumber,
                                  style: GoogleFonts.lato(
                                      fontSize: 16.0,
                                      textStyle:
                                          TextStyle(color: Colors.black))),
                            ),
                            Center(
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 1.0,
                                  color: Color(0xFF0F2037)),
                            )
                          ],
                        ),
                      )),
                  Flexible(
                      flex: 2,
                      fit: FlexFit.tight,
                      child: Center(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(Icons.chevron_right),
                          ),
                        ),
                      ))
                ],
              ),
            ),
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          Hero(
            tag: 'ProfileP',
            child: Stack(
              children: <Widget>[
                Container(
                  margin:
                      const EdgeInsets.only(top: 8.0, right: 15.0, bottom: 8.0),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 6.0,
                            spreadRadius: 4.0,
                            offset: Offset(0.0, 3.0))
                      ],
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
                          fit: BoxFit.cover)),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(
                                heroTag: 'ProfileP',
                                imgPath:
                                    'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg',
                              )))
                    },
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: Colors.white,
        leading: Icon(Icons.menu, color: Colors.black),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ListView(
            padding: EdgeInsets.only(left: 16.0, right: 16.0),
            children: <Widget>[
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Hero(
                    tag: heroTag,
                    child: Container(
                      height: 50.0,
                      width: 50.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(imgPath), fit: BoxFit.contain),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Text(
                      optionName,
                      style: GoogleFonts.lato(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          textStyle: TextStyle(color: Colors.black)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 10.0),
              _staffItem('Ronald Vega', 'IT Manager', '123 456 7890',
                  'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
              _staffItem('Salvador Vega', 'General Manager', '301 323 3213',
                  'https://minimaltoolkit.com/images/randomdata/male/87.jpg'),
              _staffItem('Julio', 'Staff Position', 'Staff Number',
                  'https://www.surrey.ac.uk/sites/default/files/styles/600x450_4_3/public/2018-02/leo-blanchard-student-graduate-profile.jpg'),
              _staffItem('Juan', 'Staff Position', 'Staff Number',
                  'https://cdn.vox-cdn.com/thumbor/wI3iu8sNbFJSQB4yMLsoPMNzIHU=/0x0:3368x3368/1200x800/filters:focal(1188x715:1726x1253)/cdn.vox-cdn.com/uploads/chorus_image/image/62994726/AJ_Finn_author_photo_color_photo_courtesy_of_the_author.0.jpg'),
              _staffItem('Jose', 'Staff Position', 'Staff Number',
                  'https://46svmh2eidr34ea1z715q5mm-wpengine.netdna-ssl.com/wp-content/uploads/Galbraith-3web-cropped-small.jpg'),
              _staffItem('Leo', 'Staff Position', 'Staff Number',
                  'https://minimaltoolkit.com/images/randomdata/male/70.jpg'),
              _staffItem('Javier', 'Staff Position', 'Staff Number',
                  'https://images.unsplash.com/photo-1544098485-2a2ed6da40ba?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80'),
              _staffItem('Staff Name', 'Staff Position', 'Staff Number',
                  'https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1442027779000/photosp/a4098098-a065-4734-8683-6d12b05e3006/stock-photo-portrait-men-profile-beard-a4098098-a065-4734-8683-6d12b05e3006.jpg'),
              _staffItem('Staff Name', 'Staff Position', '301 392 1211',
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRMRCVTr703K1P1irvGtaPhtYawAr0AjkAObKoJfuyecY62PXhw'),
            ]),
      ),
    );
  }
}
