import 'package:car_management/models/car_model.dart';
import 'package:car_management/services/firebase_storage_service.dart';
import 'package:flutter/material.dart';
import 'package:car_management/pages/details_page.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class CarListTab extends StatefulWidget {
  @override
  _CarListTabState createState() => _CarListTabState();
}

class _CarListTabState extends State<CarListTab> {
  @override
  Widget build(BuildContext context) {
    final db = Provider.of<FirebaseStorageService>(context);
    return Scaffold(
        body: StreamProvider<List<Car>>.value(
      value: db.getCars(),
      child: CarsList(),
    ));
  }
}

class CarsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var cars = Provider.of<List<Car>>(context);

    _buildListItem(String carName, rating, price, imgPath) {
      return Padding(
        padding: EdgeInsets.all(15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 220,
              child: Row(children: <Widget>[
                Flexible(
                  fit: FlexFit.tight,
                  flex: 4,
                  child: Container(
                      height: 75.0,
                      width: 75.0,
                      // decoration: BoxDecoration(
                      //     borderRadius: BorderRadius.circular(7.0),
                      //     color: Colors.grey),
                      child: Center(
                          child: Image.network(imgPath,
                              height: 75.0, width: 75.0))),
                ),
                SizedBox(
                  width: 20.0,
                ),
                Flexible(
                  fit: FlexFit.tight,
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(carName,
                          style: GoogleFonts.notoSans(
                              fontSize: 14.0, fontWeight: FontWeight.w400)),
                      SmoothStarRating(
                          allowHalfRating: false,
                          onRatingChanged: (v) {},
                          starCount: rating.toInt(),
                          rating: rating,
                          color: Color(0xFFFFD143),
                          borderColor: Color(0xFFFFD143),
                          size: 15.0,
                          spacing: 0.0),
                      Row(
                        children: <Widget>[
                          Text(
                            '\$' + price,
                            style: GoogleFonts.lato(
                                fontSize: 16.0,
                                fontWeight: FontWeight.w600,
                                textStyle: TextStyle(color: Color(0xFFF68D7F))),
                          ),
                          SizedBox(width: 3.0),
                          Text(
                            '\$' + '1200',
                            style: GoogleFonts.lato(
                                fontSize: 12.0,
                                decoration: TextDecoration.lineThrough,
                                fontWeight: FontWeight.w600,
                                textStyle: TextStyle(
                                    color: Colors.grey.withOpacity(0.4))),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ]),
            ),
            FloatingActionButton(
              heroTag: carName,
              mini: true,
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DetailsPage(
                        heroTag: carName, carName: carName, imgPath: imgPath)));
              },
              child:
                  Center(child: Icon(Icons.info_outline, color: Colors.white)),
              backgroundColor: Color(0xFF3c5e8d),
            )
          ],
        ),
      );
    }

    return cars != null
        ? ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            itemCount: cars.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildListItem(
                  cars[index].manufacturer.name + ' ' + cars[index].model,
                  4.0,
                  '600',
                  cars[index].manufacturer.logo);
            },
          )
        : Center(child: CircularProgressIndicator());
  }
}
