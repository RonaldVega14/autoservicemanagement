import 'package:car_management/changeNotifiers/addCar_notifier.dart';
import 'package:car_management/models/car_model.dart';
import 'package:car_management/models/manufacturer_model.dart';
import 'package:car_management/models/panel_model.dart';
import 'package:car_management/pages/profile_page.dart';
import 'package:car_management/services/firebase_storage_service.dart';
import 'package:car_management/utils/cutomDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AddNewCar extends StatelessWidget {
  final imgPath, optionName, heroTag;

  const AddNewCar({Key key, this.imgPath, this.optionName, this.heroTag})
      : super(key: key);

  static final formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    //Importing provider Services
    final database =
        Provider.of<FirebaseStorageService>(context, listen: false);
    var addCarNotifier = Provider.of<AddCarNotifier>(context, listen: false);
    //Keys
    final scaffoldKey = new GlobalKey<ScaffoldState>();
    //Obtaining Manufacturers.
    List<Manufacturer> manufacturers = Provider.of<List<Manufacturer>>(context);
    //Strings utilizados en la form, 1 String por cada campo en la form
    String _carModel,
        _carYear,
        _carProvider,
        _vinNumber,
        _carColor,
        _receivedDate;
    Manufacturer _carManufacturer = new Manufacturer(name: '', logo: '');
    _showManufacturers(BuildContext context) {
      print('Manufacturers: ' + manufacturers.length.toString());

      showModalBottomSheet(
          context: context,
          builder: (context) {
            return manufacturers != null
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Select Car Manufacturer',
                              textAlign: TextAlign.center,
                              style: GoogleFonts.lato(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w400,
                                  textStyle:
                                      TextStyle(color: Color(0xFF0F2037))),
                            ),
                          ),
                          flex: 1,
                          fit: FlexFit.tight,
                        ),
                        Flexible(
                            child: ListView.builder(
                                itemCount: manufacturers.length,
                                itemBuilder: (context, index) {
                                  return ListTile(
                                    onTap: () {
                                      Navigator.pop(context);
                                      _carManufacturer.name =
                                          manufacturers[index].name;
                                      _carManufacturer.logo =
                                          manufacturers[index].logo;
                                      print(_carManufacturer.name);
                                      addCarNotifier.changeManufacturer(
                                          _carManufacturer.name);
                                    },
                                    leading: Container(
                                      height: 80,
                                      width: 80,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                            child: Image.network(
                                                manufacturers[index].logo,
                                                fit: BoxFit.contain)),
                                      ),
                                    ),
                                    title: Text(
                                      manufacturers[index].name,
                                      style: GoogleFonts.lato(
                                          fontSize: 18,
                                          textStyle:
                                              TextStyle(color: Colors.black)),
                                    ),
                                  );
                                }),
                            fit: FlexFit.tight,
                            flex: 10)
                      ])
                : Center(child: CircularProgressIndicator());
          });
    }

    //Manufacturer Input Field
    Widget manufacturerButton() {
      return Center(
        child: MaterialButton(
            elevation: 3.0,
            minWidth: MediaQuery.of(context).size.width * 0.4,
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(12.0),
            ),
            textColor: Colors.white,
            padding: const EdgeInsets.fromLTRB(25.0, 10, 25.0, 10),
            color: Color(0xFF0F2037),
            child: Text(
              'Select Manufacturer',
              style: GoogleFonts.lato(
                  fontSize: 16.0,
                  textStyle: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.w600)),
            ),
            onPressed: () => _showManufacturers(
                  context,
                )),
      );
    }

    //ManufacturerName
    Widget manufacturerField() {
      return Row(children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 0, 5.0, 0),
          child: Text(
            'Car Manufacturer: ',
            style: GoogleFonts.lato(
                fontSize: 15.0, textStyle: TextStyle(color: Color(0xFF696969))),
          ),
        ),
        Consumer<AddCarNotifier>(builder: (_, addCarNotifier, __) {
          return Text(
            addCarNotifier.getManufacturer(),
            style: GoogleFonts.lato(
                fontSize: 16.0,
                textStyle: TextStyle(
                    color: Color(0xFF696969), fontWeight: FontWeight.bold)),
          );
        }),
      ]);
    }

    //Car Model Input Field
    final modelField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _carModel = val,
      style: GoogleFonts.lato(
          fontSize: 15.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Car Model',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Car Color Input Field
    final colorField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _carColor = val,
      style: GoogleFonts.lato(
          fontSize: 16.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Car color',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Car Year Input Field
    final yearField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _carYear = val,
      style: GoogleFonts.lato(
          fontSize: 16.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Car Year',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Car Provider Input Field
    final providerField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _carProvider = val,
      style: GoogleFonts.lato(
          fontSize: 15.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Car Provider',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Vin Number Input Field
    final vinField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _vinNumber = val,
      style: GoogleFonts.lato(
          fontSize: 15.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Car Vin Number',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Recieved Date Input Field
    final recievedDateField = TextFormField(
      validator: (val) => val.isEmpty ? 'Required Field' : null,
      onSaved: (val) => _receivedDate = val,
      style: GoogleFonts.lato(
          fontSize: 15.0, textStyle: TextStyle(color: Color(0xff516c8d))),
      decoration: InputDecoration(
          labelText: 'Recieved Date',
          contentPadding: EdgeInsets.fromLTRB(20.0, 0, 15.0, 0),
          border: UnderlineInputBorder()),
    );
    //Damaged Panels Input Fields
    Widget _multipleInputField(String validatorString, int index, String label,
        List<Panel> onSavedArray) {
      var panelTemp = onSavedArray[index];
      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
                flex: 12,
                fit: FlexFit.tight,
                child: TextFormField(
                  validator: (val) => val.isEmpty ? validatorString : null,
                  onSaved: (val) {
                    panelTemp.name = val;
                  },
                  style: GoogleFonts.lato(
                      fontSize: 15.0,
                      textStyle: TextStyle(color: Color(0xff516c8d))),
                  decoration: InputDecoration(
                      labelText: label,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 0, 5.0, 0),
                      border: UnderlineInputBorder()),
                )),
            Flexible(
              flex: 2,
              fit: FlexFit.loose,
              child: Center(
                child: IconButton(
                    icon: Icon(
                        panelTemp.image == null
                            ? Icons.add_a_photo
                            : Icons.check_circle_outline,
                        color: Colors.grey.withOpacity(0.8)),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (_) => CustomSelectImageWidget(
                          image: onSavedArray[index].image,
                          index: index,
                          addCarNotifier: addCarNotifier,
                        ),
                      );
                    }),
              ),
            )
          ]);
    }

    Widget _damagedPanelsFields() {
      return Consumer<AddCarNotifier>(builder: (_, addCarNotifier, __) {
        return Column(children: <Widget>[
          ListView.builder(
            shrinkWrap: true,
            itemCount: addCarNotifier.getPanelsInfo().length,
            itemBuilder: (BuildContext context, int index) {
              return Column(children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: _multipleInputField('Invalid Panel Name', index,
                      'Damaged Panel', addCarNotifier.getPanelsInfo()),
                ),
              ]);
            },
          ),
          Center(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                addCarNotifier.getDamagedPanels().length > 1
                    ? Container(
                        height: 35,
                        margin: const EdgeInsets.all(8.0),
                        child: FloatingActionButton(
                          child: Icon(
                            Icons.remove,
                            color: Colors.white,
                            size: 25,
                          ),
                          onPressed: () {
                            addCarNotifier.removePanelField();
                          },
                          backgroundColor: Color(0xff516c8d),
                        ),
                      )
                    : SizedBox(),
                Container(
                  height: 35,
                  margin: const EdgeInsets.all(8.0),
                  child: FloatingActionButton(
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 25,
                    ),
                    onPressed: () {
                      addCarNotifier.addPanelField(_multipleInputField(
                          'Invalid Panel Name',
                          addCarNotifier.getDamagedPanels().length - 1,
                          'Damaged Panel',
                          addCarNotifier.getPanelsInfo()));
                    },
                    backgroundColor: Color(0xFF0F2037),
                  ),
                )
              ])),
        ]);
      });
    }

    Future<bool> uploadCar() async {
      bool uploadSuccess = false;
      Car newCar = Car(
          id: '',
          manufacturer: _carManufacturer,
          model: _carModel,
          color: _carColor,
          year: _carYear,
          provider: _carProvider,
          vin: _vinNumber,
          recievedDate: _receivedDate,
          damagedPanels: []);
      uploadSuccess = await database.uploadCarData(
          addCarNotifier.getPanelsInfo(), newCar, context);
      //Todo for present result screen
      if (uploadSuccess) {
        return true;
      } else {
        print('FUCK!');
        return false;
      }
    }

    Widget addCarButton() {
      return Center(
        child: MaterialButton(
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(12.0),
          ),
          textColor: Colors.white,
          color: Color(0xFF0F2037),
          elevation: 5.0,
          minWidth: MediaQuery.of(context).size.width * 0.5,
          padding: EdgeInsets.fromLTRB(5.0, 10.0, 10.0, 10.0),
          onPressed: () async {
            if (formKey.currentState.validate()) {
              formKey.currentState.save();
              bool uploaded = await uploadCar();
              showDialog(
                  context: context,
                  builder: (_) => Center(
                      child: FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Container(
                              width: MediaQuery.of(context).size.width * .65,
                              height: MediaQuery.of(context).size.height * .35,
                              padding: const EdgeInsets.fromLTRB(
                                  25.0, 5.0, 25.0, 13.0),
                              margin: const EdgeInsets.fromLTRB(
                                  25.0, 16.0, 25.0, 12.0),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(24.0),
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        spreadRadius: 2.0, color: Colors.white)
                                  ]),
                              child: Center(
                                child: CircularProgressIndicator(),
                              )))));

              if (uploaded) {
                showDialog(
                  context: context,
                  builder: (_) => CustomDialogWidget(
                    msg: 'Car added Successfully!',
                    answer: uploaded,
                  ),
                );
              } else {
                Navigator.of(context).pop();
                showDialog(
                  context: context,
                  builder: (_) => CustomDialogWidget(
                    msg:
                        'An error has ocurred while uploading the information, try again later',
                    answer: uploaded,
                  ),
                );
              }
            }
          },
          child: Text("Add Car",
              textAlign: TextAlign.center,
              style: GoogleFonts.lato(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  textStyle: TextStyle(color: Colors.white))),
        ),
      );
    }

    Widget _inputInformation() {
      return Form(
        key: formKey,
        child: Center(
          child: ListView(
            padding: EdgeInsets.only(left: 32.0, right: 32.0),
            children: <Widget>[
              SizedBox(height: 20.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Hero(
                    tag: heroTag,
                    child: Container(
                      height: 50.0,
                      width: 50.0,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(imgPath), fit: BoxFit.contain),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: Text(
                      optionName,
                      style: GoogleFonts.lato(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                          textStyle: TextStyle(color: Colors.black)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 25.0),
              manufacturerField(),
              SizedBox(height: 15.0),
              manufacturerButton(),
              SizedBox(height: 15.0),
              modelField,
              SizedBox(height: 10.0),
              colorField,
              SizedBox(height: 10.0),
              yearField,
              SizedBox(height: 10.0),
              providerField,
              SizedBox(height: 10.0),
              vinField,
              SizedBox(height: 10.0),
              recievedDateField,
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Text(
                  'DAMAGED PARTS',
                  style: GoogleFonts.notoSans(
                      fontSize: 16.0, fontWeight: FontWeight.w700),
                ),
              ),
              SizedBox(height: 10.0),
              _damagedPanelsFields(),
              SizedBox(height: 25.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: addCarButton(),
              ),
              SizedBox(height: 25.0),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          Hero(
            tag: 'ProfileP',
            child: Stack(
              children: <Widget>[
                Container(
                  margin:
                      const EdgeInsets.only(top: 8.0, right: 15.0, bottom: 8.0),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 6.0,
                            spreadRadius: 4.0,
                            offset: Offset(0.0, 3.0))
                      ],
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
                          fit: BoxFit.cover)),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(
                                heroTag: 'ProfileP',
                                imgPath:
                                    'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg',
                              )))
                    },
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: Colors.white,
        leading: Icon(Icons.menu, color: Colors.black),
      ),
      backgroundColor: Colors.white,
      key: scaffoldKey,
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: _inputInformation(),
      ),
    );
  }
}
