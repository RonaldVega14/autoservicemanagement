import 'package:car_management/changeNotifiers/addCar_notifier.dart';
import 'package:car_management/models/manufacturer_model.dart';
import 'package:car_management/pages/addCar_page.dart';
import 'package:car_management/pages/carList_tab.dart';
import 'package:car_management/pages/profile_page.dart';
import 'package:car_management/pages/staff_page.dart';
import 'package:car_management/services/firebase_auth_service.dart';
import 'package:car_management/services/firebase_storage_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(vsync: this, length: 4);
  }

  Future<void> _signOut(BuildContext context) async {
    try {
      final auth = Provider.of<FirebaseAuthService>(context, listen: false);
      await auth.signOut();
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          Hero(
            tag: 'ProfileP',
            child: Stack(
              children: <Widget>[
                Container(
                  margin:
                      const EdgeInsets.only(top: 8.0, right: 15.0, bottom: 8.0),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 6.0,
                            spreadRadius: 4.0,
                            offset: Offset(0.0, 3.0))
                      ],
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
                          fit: BoxFit.cover)),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(
                                heroTag: 'ProfileP',
                                imgPath:
                                    'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg',
                              )))
                    },
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: () => _signOut(context),
            child: Icon(Icons.power_settings_new, color: Colors.red)),
      ),
      body: ListView(
        children: <Widget>[
          //Title
          Padding(
            padding: EdgeInsets.all(15.0),
            child: Text(
              'SEARCH FOR CARS',
              style: GoogleFonts.notoSans(
                  fontWeight: FontWeight.w800, fontSize: 27.0),
            ),
          ),
          //End of title
          SizedBox(
            height: 20.0,
          ),
          //Search bar
          Padding(
            padding: EdgeInsets.only(left: 15.0, right: 15.0),
            child: Container(
              padding: EdgeInsets.only(left: 5.0),
              decoration: BoxDecoration(color: Colors.grey.withOpacity(0.2)),
              child: TextField(
                  decoration: InputDecoration(
                      hintText: 'Search',
                      hintStyle: GoogleFonts.notoSans(fontSize: 14.0),
                      border: InputBorder.none,
                      fillColor: Colors.grey.withOpacity(0.5),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.grey,
                      ))),
            ),
          ),
          //End of Search bar
          SizedBox(
            height: 20.0,
          ),
          //Horizontal list view
          Container(
            height: 200.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                _buildListItem('ADD NEW CAR', 'assets/car.png',
                    Color(0xFF0F2037), Colors.white, 1),
                _buildListItem('VIEW CAR STATUS', 'assets/car-maintenance.png',
                    Color(0xff28385e), Colors.white, 2),
                _buildListItem('MANAGE STAFF', 'assets/car-staff.png',
                    Color(0xff516c8d), Colors.white, 3),
                SizedBox(width: 15.0)
              ],
            ),
          ),
          //End of horizontal list view
          SizedBox(
            height: 25.0,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15.0),
            child: TabBar(
                controller: tabController,
                isScrollable: true,
                indicatorColor: Color(0xFF0F2037),
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey.withOpacity(0.7),
                labelStyle: GoogleFonts.notoSans(
                    fontSize: 16.0, fontWeight: FontWeight.w700),
                unselectedLabelStyle: GoogleFonts.notoSans(
                    fontSize: 12.0, fontWeight: FontWeight.w500),
                tabs: [
                  Tab(child: Text('ALL CARS')),
                  Tab(child: Text('BORIS')),
                  Tab(child: Text('ELITE AUTO')),
                  Tab(child: Text('MINI')),
                ]),
          ),
          Container(
              height: MediaQuery.of(context).size.height - 100.00,
              child: TabBarView(controller: tabController, children: [
                CarListTab(),
                CarListTab(),
                CarListTab(),
                CarListTab(),
              ])),
        ],
      ),
    );
  }

  _buildListItem(String optionName, String imgPath, Color bgColor,
      Color textColor, int menuOption) {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, bottom: 5.0),
      child: InkWell(
        onTap: () {
          switch (menuOption) {
            case 1:
              final database =
                  Provider.of<FirebaseStorageService>(context, listen: false);
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => MultiProvider(
                          providers: [
                            ChangeNotifierProvider<AddCarNotifier>(
                              create: (context) => AddCarNotifier(),
                            ),
                            StreamProvider<List<Manufacturer>>.value(
                              value: database.getManufacturers(),
                            )
                          ],
                          child: AddNewCar(
                              heroTag: optionName,
                              optionName: optionName,
                              imgPath: imgPath))));
              break;
            case 3:
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => StaffPage(
                        heroTag: optionName,
                        optionName: optionName,
                        imgPath: imgPath,
                      )));
              break;
            default:
              print(optionName + ' Pressed');
          }
        },
        child: Material(
          elevation: 2.0,
          color: bgColor.withOpacity(0.9),
          borderRadius: BorderRadius.circular(15.0),
          child: Container(
            height: 175.00,
            width: 150.00,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  flex: 7,
                  fit: FlexFit.loose,
                  child: Hero(
                    tag: optionName,
                    child: Container(
                      height: 75.0,
                      width: 75.0,
                      decoration: BoxDecoration(
                          color: Colors.white, shape: BoxShape.circle),
                      child: Center(
                        child: Image.asset(imgPath, height: 53.0, width: 53.0),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 25.0),
                Flexible(
                  flex: 3,
                  fit: FlexFit.tight,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      optionName,
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.notoSans(
                          fontSize: 17.0, color: textColor),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
