import 'package:car_management/pages/profile_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:table_calendar/table_calendar.dart';

class ManageSchedulePage extends StatefulWidget {
  final heroTag, staffName, staffPosition, imgPath;

  const ManageSchedulePage(
      {Key key,
      @required this.heroTag,
      @required this.staffName,
      @required this.imgPath,
      @required this.staffPosition})
      : super(key: key);
  @override
  _ManageSchedulePageState createState() => _ManageSchedulePageState();
}

class _ManageSchedulePageState extends State<ManageSchedulePage>
    with TickerProviderStateMixin {
  Map<DateTime, List> _events = {};
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;
  DateTime _fromDate, _toDate;
  DateTime timeIn = DateTime.now().subtract(Duration(minutes: 1)),
      timeOut = DateTime.now();
  bool _range = false;

  Map<DateTime, List> _holidays = {};

  @override
  void initState() {
    super.initState();
    final _selectedDay = DateTime.now();
    _fromDate = _selectedDay;
    _toDate = _fromDate;
    _holidays.addAll({
      _fromDate: ['from'],
    });

    // _events = {
    //   _selectedDay.subtract(Duration(days: 1)): [
    //     'Event A0',
    //     'Event B0',
    //   ],
    //   _selectedDay.subtract(Duration(days: 3)): ['Event A1'],
    //   _selectedDay.subtract(Duration(days: 5)): [
    //     'Event A2',
    //     'Event B2',
    //     'Event C2',
    //     'Event D2'
    //   ],
    //   _selectedDay.subtract(Duration(days: 8)): ['Event A3', 'Event B3'],
    //   _selectedDay.subtract(Duration(days: 9)): [
    //     'Event A4',
    //     'Event B4',
    //     'Event C4'
    //   ],
    //   _selectedDay.subtract(Duration(days: 10)): [
    //     'Event A5',
    //     'Event B5',
    //     'Event C5'
    //   ],
    //   _selectedDay.subtract(Duration(days: 12)): ['Event A6', 'Event B6'],
    //   _selectedDay: ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
    //   _selectedDay.add(Duration(days: 2)): [
    //     'Event A8',
    //     'Event B8',
    //     'Event C8',
    //     'Event D8'
    //   ],
    //   _selectedDay.add(Duration(days: 3)):
    //       Set.from(['Event A9', 'Event A9', 'Event B9']).toList(),
    //   _selectedDay.add(Duration(days: 4)): [
    //     'Event A10',
    //     'Event B10',
    //     'Event C10'
    //   ],
    //   _selectedDay.add(Duration(days: 8)): ['Event A11', 'Event B11'],
    //   _selectedDay.add(Duration(days: 10)): [
    //     'Event A12',
    //     'Event B12',
    //     'Event C12',
    //     'Event D12'
    //   ],
    //   _selectedDay.add(Duration(days: 13)): ['Event A13', 'Event B13'],
    //   _selectedDay.add(Duration(days: 15)): [
    //     'Event A14',
    //     'Event B14',
    //     'Event C14'
    //   ],
    // };

    _selectedEvents = _events[_selectedDay] ?? [];
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events, bool range) {
    print('CALLBACK: _onDaySelected');
    if (range) {
      int _daysInRange = day.day - _fromDate.day;
      setState(() {
        _toDate = day;
        _selectedEvents = events;
        _range = !range;
        for (var i = 0; i < _daysInRange; i++) {
          _holidays.addAll({
            _fromDate.add(Duration(days: i)): ['from'],
          });
        }
      });
    } else {
      setState(() {
        _fromDate = day;
        _selectedEvents = events;
        _range = !range;
        _holidays.clear();
        _holidays.addAll({
          _fromDate: ['from'],
        });
      });
    }
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          Hero(
            tag: 'ProfileP',
            child: Stack(
              children: <Widget>[
                Container(
                  margin:
                      const EdgeInsets.only(top: 8.0, right: 15.0, bottom: 8.0),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 6.0,
                            spreadRadius: 4.0,
                            offset: Offset(0.0, 3.0))
                      ],
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
                          fit: BoxFit.cover)),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(
                                heroTag: 'ProfileP',
                                imgPath:
                                    'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg',
                              )))
                    },
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: Colors.white,
        leading: Icon(Icons.menu, color: Colors.black),
      ),
      body: ListView(
        children: <Widget>[
          // Switch out 2 lines below to play with TableCalendar's settings
          //-----------------------
          //_buildTableCalendar(),
          _buildTableCalendarWithBuilders(),
          const SizedBox(height: 8.0),
          //_buildButtons(),
          const SizedBox(height: 8.0),
          _buildBottomList(),
        ],
      ),
    );
  }

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {
    return TableCalendar(
      locale: 'en_US',
      calendarController: _calendarController,
      events: _events,
      holidays: _holidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.twoWeeks: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        weekendStyle: TextStyle().copyWith(color: Color(0xffdd6b4d)),
        outsideDaysVisible: false,
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Color(0xffdd6b4d)),
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Color(0xff28385e),
          borderRadius: BorderRadius.circular(2.0),
        ),
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              decoration: BoxDecoration(
                color: Color(0xff28385e),
                borderRadius: BorderRadius.circular(12.0),
              ),
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style:
                    TextStyle().copyWith(fontSize: 16.0, color: Colors.white),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            decoration: BoxDecoration(
              color: Color(0xffdd6b4d).withOpacity(0.7),
              borderRadius: BorderRadius.circular(12.0),
            ),
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0, color: Colors.white),
            ),
          );
        },
        holidayDayBuilder: (context, date, holydays) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            decoration: BoxDecoration(
              color: Colors.green.withOpacity(0.7),
              borderRadius: BorderRadius.circular(12.0),
            ),
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0, color: Colors.white),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }
          return children;
        },
      ),
      onDaySelected: (date, events) {
        _onDaySelected(date, events, _range);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _calendarController.isSelected(date)
            ? Color(0xffdd6b4d)
            : _calendarController.isToday(date)
                ? Color(0xff28385e)
                : Color(0xff28385e),
      ),
      width: 18.0,
      height: 18.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 13.0,
          ),
        ),
      ),
    );
  }

  // Widget _buildButtons() {
  //   final dateTime = _events.keys.elementAt(_events.length - 2);

  //   return Column(
  //     children: <Widget>[
  //       Row(
  //         mainAxisSize: MainAxisSize.max,
  //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //         children: <Widget>[
  //           RaisedButton(
  //             padding: const EdgeInsets.all(8.0),
  //             child: Text('Month'),
  //             onPressed: () {
  //               setState(() {
  //                 _calendarController.setCalendarFormat(CalendarFormat.month);
  //               });
  //             },
  //           ),
  //           RaisedButton(
  //             child: Text('2 weeks'),
  //             onPressed: () {
  //               setState(() {
  //                 _calendarController
  //                     .setCalendarFormat(CalendarFormat.twoWeeks);
  //               });
  //             },
  //           ),
  //           RaisedButton(
  //             child: Text('Week'),
  //             onPressed: () {
  //               setState(() {
  //                 _calendarController.setCalendarFormat(CalendarFormat.week);
  //               });
  //             },
  //           ),
  //         ],
  //       ),
  //       const SizedBox(height: 8.0),
  //       RaisedButton(
  //         child: Text(
  //             'Set day ${dateTime.day}-${dateTime.month}-${dateTime.year}'),
  //         onPressed: () {
  //           _calendarController.setSelectedDay(
  //             DateTime(dateTime.year, dateTime.month, dateTime.day),
  //             runCallback: true,
  //           );
  //         },
  //       ),
  //     ],
  //   );
  // }

  Widget _buildBottomList() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: widget.heroTag,
              child: _bottomItem(
                  widget.imgPath, widget.staffName, widget.staffPosition, 0),
            ),
            _bottomItem(
                'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/calendar-512.png',
                'Selected Dates',
                'From: ${_fromDate.day}-${_fromDate.month}-${_fromDate.year} \nTo: ${_toDate.day}-${_toDate.month}-${_toDate.year}',
                1),
            _bottomItem(
                'https://upload.wikimedia.org/wikipedia/commons/thumb/3/30/Icons8_flat_clock.svg/1024px-Icons8_flat_clock.svg.png',
                'Select Hours',
                'From: ${timeIn.toString().split(' ').elementAt(1).split('.').elementAt(0)} - To: ${timeOut.toString().split(' ').elementAt(1).split('.').elementAt(0)}',
                2),
            SizedBox(height: 20),
            _finishButton(),
            SizedBox(height: 15.0)
          ]),
    );
  }

  _showModalBottomSheet(BuildContext context, String title, bool showModal) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.9,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25))),
            child: Column(children: <Widget>[
              Flexible(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(title,
                          style: GoogleFonts.lato(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              textStyle: TextStyle(color: Colors.black))),
                    ),
                  ),
                  flex: 2,
                  fit: FlexFit.tight),
              Flexible(
                flex: 9,
                fit: FlexFit.tight,
                child: Stack(children: <Widget>[
                  CupertinoDatePicker(
                      initialDateTime: timeIn,
                      mode: CupertinoDatePickerMode.time,
                      use24hFormat: false,
                      onDateTimeChanged: (dateTime) {
                        if (showModal) {
                          timeIn = dateTime;
                        } else {
                          timeOut = dateTime;
                        }
                      }),
                  PositionedDirectional(
                    end: 16.0,
                    bottom: 16.0,
                    child: FloatingActionButton(
                      mini: true,
                      onPressed: () {
                        Navigator.pop(context);
                        showModal
                            ? _showModalBottomSheet(
                                context, 'Choose Clock Out Time', false)
                            : setState(() {});
                      },
                      child: Center(
                          child: Icon(Icons.arrow_right, color: Colors.white)),
                      backgroundColor: Color(0xFF3c5e8d),
                    ),
                  )
                ]),
              ),
            ]),
          );
        });
  }

  Widget _bottomItem(String imgPath, String optionTitle, String optionSubtitle,
      int optionNumber) {
    return Center(
      child: GestureDetector(
        onTap: (() {
          switch (optionNumber) {
            case 1:
              break;
            case 2:
              _showModalBottomSheet(context, 'Choose Clock In Time', true);
              break;
            default:
          }
        }),
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 5.0),
          decoration: BoxDecoration(
              color: Color(0xfff6f5f5),
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    blurRadius: 3.0,
                    spreadRadius: 3.0,
                    offset: Offset(0.0, 3.0))
              ]),
          height: MediaQuery.of(context).size.height / 8,
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Flexible(
                flex: 4,
                fit: FlexFit.loose,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.transparent,
                      image: DecorationImage(
                          image: NetworkImage(imgPath), fit: BoxFit.cover),
                      borderRadius: BorderRadius.all(Radius.circular(80.0)),
                    ),
                  ),
                ),
              ),
              Flexible(
                  flex: 12,
                  fit: FlexFit.tight,
                  child: Container(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: Text(optionTitle,
                              style: GoogleFonts.lato(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w600,
                                  textStyle: TextStyle(color: Colors.black))),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 2.0),
                          child: Text(optionSubtitle,
                              style: GoogleFonts.lato(
                                  fontSize: 15.0,
                                  textStyle: TextStyle(color: Colors.black54))),
                        ),
                      ],
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _finishButton() {
    return Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
      Container(
        width: MediaQuery.of(context).size.width * 0.6,
        child: Material(
          shadowColor: Colors.black,
          elevation: 5.0,
          borderRadius: BorderRadius.circular(12.0),
          color: Color(0xff28385e),
          child: MaterialButton(
            padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
            onPressed: () => {
              print(
                  'Schedule Will be added to ${widget.staffName}\nDates\nFrom: ${_fromDate.toString().split(' ').elementAt(0)} - To: ${_toDate.toString().split(' ').elementAt(0)}\nHours\nFrom: ${timeIn.toString()} - To: ${timeOut.toString()}')
            },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 20.0,
                    width: 20.0,
                    child: Center(
                      child: Image.asset('assets/calendar-done.png',
                          height: 20.0, width: 20.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("FINISH SCHEDULE",
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: GoogleFonts.lato(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w600,
                            textStyle: TextStyle(color: Colors.white))),
                  )
                ]),
          ),
        ),
      ),
    ]);
  }
}
