import 'package:car_management/pages/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DetailsPage extends StatefulWidget {
  final imgPath, carName, heroTag;

  const DetailsPage({Key key, this.imgPath, this.carName, this.heroTag})
      : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  var netPrice = 0.0;
  var quantity = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        actions: <Widget>[
          Hero(
            tag: 'ProfileP',
            child: Stack(
              children: <Widget>[
                Container(
                  margin:
                      const EdgeInsets.only(top: 8.0, right: 15.0, bottom: 8.0),
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.3),
                            blurRadius: 6.0,
                            spreadRadius: 4.0,
                            offset: Offset(0.0, 3.0))
                      ],
                      color: Colors.transparent,
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg'),
                          fit: BoxFit.cover)),
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProfilePage(
                                heroTag: 'ProfileP',
                                imgPath:
                                    'https://content.thriveglobal.com/wp-content/uploads/2018/12/profile21.jpg',
                              )))
                    },
                  ),
                ),
              ],
            ),
          )
        ],
        backgroundColor: Colors.white,
        leading: Icon(Icons.menu, color: Colors.black),
      ),
      body: ListView(
        children: <Widget>[
          //Start Title name
          Padding(
            padding: EdgeInsets.only(top: 15.0, left: 15.0),
            child: Text(widget.carName.toString().toUpperCase(),
                style: GoogleFonts.notoSans(
                    fontWeight: FontWeight.w800, fontSize: 27.0)),
          ),
          //End Title name
          SizedBox(
            height: 40.0,
          ),
          //Start mid section
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              //Car Logo
              Hero(
                tag: widget.heroTag,
                child: Container(
                  height: 200.0,
                  width: 200.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(widget.imgPath), fit: BoxFit.contain),
                  ),
                ),
              ),
              //End of Car Logo
              SizedBox(
                width: 15.0,
              ),
              //Buttons at the right of the car logo
              Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 45.0,
                        width: 40.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFF3c5e8d).withOpacity(0.1),
                                  blurRadius: 6.0,
                                  spreadRadius: 6.0,
                                  offset: Offset(5.0, 11.0))
                            ]),
                      ),
                      Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            color: Colors.white),
                        child: Center(
                          child: Icon(
                            Icons.photo_library,
                            color: Color(0xFF3c5e8d),
                            size: 25.0,
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 35.0,
                  ),
                  Stack(
                    children: <Widget>[
                      Container(
                        height: 45.0,
                        width: 40.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15.0),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFF3c5e8d).withOpacity(0.1),
                                blurRadius: 6.0,
                                spreadRadius: 6.0,
                                offset: Offset(5.0, 11.0)),
                          ],
                        ),
                      ),
                      Container(
                        height: 50.0,
                        width: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12.0),
                          color: Colors.white,
                        ),
                        child: Center(
                          child: Icon(
                            Icons.restore,
                            color: Color(0xFF3c5e8d),
                            size: 25.0,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              )
              //End of buttons at the right of car logo
            ],
          ),
          SizedBox(
            height: 10,
          ),
          //Start of bar at the bottom of the car logo
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 60.0,
                width: 225.0,
                decoration: BoxDecoration(
                    color: Color(0xFF3c5e8d),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(12.0),
                        bottomRight: Radius.circular(12.0))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      'Status',
                      style: GoogleFonts.notoSans(
                          fontSize: 15.0,
                          color: Colors.white,
                          fontWeight: FontWeight.w400),
                    ),
                    Container(
                      height: 40.0,
                      width: 115.00,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Prep Primer',
                            style: GoogleFonts.notoSans(
                                fontSize: 15.0,
                                color: Color(0xFF3c5e8d),
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          //Start of the botto section
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              'DAMAGED PANELS',
              style: GoogleFonts.notoSans(
                  fontSize: 16.0, fontWeight: FontWeight.w700),
            ),
          ),
          Container(
              height: 225.0,
              width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[_buildListItem('1'), _buildListItem('2')],
              ))
        ],
      ),
    );
  }

  _buildListItem(String columnNumber) {
    return Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            //Don't do this in a real app.
            if (columnNumber == '1')
              _buildColumnItem('assets/tuxedo.png', 'Bumper', 'Not Started',
                  Color(0xFF3c5e8d)),
            if (columnNumber == '1')
              SizedBox(height: 15.0),
            if (columnNumber == '1')
              _buildColumnItem('assets/tuxedo.png', 'Hood', 'In progress',
                  Color(0xFF3c5e8d)),
            if (columnNumber == '2')
              _buildColumnItem('assets/tuxedo.png', 'Right Door', 'Finished',
                  Color(0xFF3c5e8d)),
            if (columnNumber == '2')
              SizedBox(height: 15.0),
            if (columnNumber == '2')
              _buildColumnItem('assets/tuxedo.png', 'Left Door', 'Finished',
                  Color(0xFF3c5e8d)),
          ],
        ));
  }

  _buildColumnItem(
      String imgPath, String carName, String process, Color bgColor) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            width: 210.0,
            child: Row(children: [
              Container(
                  height: 75.0,
                  width: 75.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(7.0), color: bgColor),
                  child: Center(
                      child: Image.asset(imgPath, height: 50.0, width: 50.0))),
              SizedBox(width: 20.0),
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                Text(carName,
                    style: GoogleFonts.notoSans(
                        fontSize: 14.0, fontWeight: FontWeight.w400)),
                Text(
                  process,
                  style: GoogleFonts.lato(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600,
                      textStyle: TextStyle(color: Color(0xFFF68D7F))),
                ),
              ])
            ]))
      ],
    );
  }

  adjustQuantity(pressed) {
    switch (pressed) {
      case 'PLUS':
        setState(() {
          quantity += 1;
        });
        return;
      case 'MINUS':
        setState(() {
          if (quantity != 0) {
            quantity -= 1;
          }
        });
        return;
    }
  }
}
