import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ProfilePage extends StatelessWidget {
  final heroTag, imgPath;

  const ProfilePage({Key key, @required this.heroTag, @required this.imgPath})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    Widget scheduleButton() => Container(
          width: MediaQuery.of(context).size.width * 0.5,
          child: Material(
            shadowColor: Colors.black,
            elevation: 5.0,
            borderRadius: BorderRadius.circular(12.0),
            color: Color(0xff28385e),
            child: MaterialButton(
              padding: EdgeInsets.fromLTRB(20.0, 3.0, 20.0, 3.0),
              onPressed: () => {},
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      Icons.calendar_today,
                      color: Colors.white,
                      size: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("MY SCHEDULE",
                          softWrap: true,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.lato(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              textStyle: TextStyle(color: Colors.white))),
                    )
                  ]),
            ),
          ),
        );

    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.arrow_back),
          )
        ],
        backgroundColor: Color(0xff28385e),
        leading: Icon(Icons.menu, color: Colors.white),
        elevation: 0.0,
      ),
      body: Stack(children: <Widget>[
        ClipPath(
          child: Container(color: Color(0xff28385e)),
          clipper: GetClipper(),
        ),
        Positioned(
            width: MediaQuery.of(context).size.width,
            top: MediaQuery.of(context).size.height / 9.0,
            child: Column(
              children: <Widget>[
                Hero(
                  tag: heroTag,
                  child: Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        image: DecorationImage(
                            image: NetworkImage(imgPath), fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(Radius.circular(75.0)),
                        boxShadow: [
                          BoxShadow(
                              blurRadius: 6.0,
                              color: Colors.black54,
                              spreadRadius: 2.0)
                        ]),
                  ),
                ),
                SizedBox(height: 20.0),
                Text(
                  'Ronald Vega',
                  style: GoogleFonts.notoSans(
                      fontSize: 28.0, fontWeight: FontWeight.w400),
                ),
                SizedBox(height: 10.0),
                Text('IT Manager',
                    style: GoogleFonts.notoSans(
                        fontSize: 17.0,
                        fontStyle: FontStyle.italic,
                        color: Colors.green)),
                SizedBox(height: 20.0),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.center,
                              //   children: <Widget>[
                              //     Padding(
                              //       padding: const EdgeInsets.only(
                              //           top: 10.0, left: 8.0, bottom: 4.0),
                              //       child: Icon(Icons.phone, size: 17),
                              //     ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 8.0,
                                    left: 8.0,
                                    right: 8.0,
                                    bottom: 2.0),
                                child: Text('123 456 7890',
                                    style: GoogleFonts.notoSans(
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                              //   ],
                              // ),
                              Text('PHONE NUMBER',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.notoSans(
                                      fontSize: 17.0, color: Colors.black54))
                            ]),
                      ),
                      Flexible(
                        flex: 2,
                        fit: FlexFit.tight,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.center,
                              //   children: <Widget>[
                              //     Padding(
                              //       padding: const EdgeInsets.only(
                              //           top: 10.0, left: 8.0, bottom: 4.0),
                              //       child: Icon(Icons.email, size: 17),
                              //     ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 8.0,
                                    left: 8.0,
                                    right: 8.0,
                                    bottom: 2.0),
                                child: Text('123456789012',
                                    style: GoogleFonts.notoSans(
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                              //   ],
                              // ),
                              Text('SSN',
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.notoSans(
                                      fontSize: 17.0, color: Colors.black54))
                            ]),
                      ),
                    ]),
                SizedBox(height: 20.0),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, left: 8.0, right: 8.0, bottom: 2.0),
                            child: Text('SEPTEMBER 17, 2019.',
                                style: GoogleFonts.notoSans(
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                        ],
                      ),
                      Text('DATE OF HIRE',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.notoSans(
                              fontSize: 17.0, color: Colors.black54))
                    ]),
                SizedBox(height: 50.0),
                scheduleButton(),
              ],
            ))
      ]),
    );
  }
}

class GetClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height / 3.0);
    path.lineTo(size.width, size.height / 6.5);
    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
