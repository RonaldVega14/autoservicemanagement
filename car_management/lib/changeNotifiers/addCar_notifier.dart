import 'dart:io';

import 'package:car_management/models/panel_model.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AddCarNotifier with ChangeNotifier {
  String manufacturer = "",
      model = "",
      color = "",
      year = "",
      provider = "",
      vin = "",
      recievedDate = "";

  @override
  void notifyListeners() {
    super.notifyListeners();
  }

  getDamagedPanels() => _damagedPanels;

  List<Panel> _panelsInfo = [new Panel(name: null, image: null)];

  List<Panel> getPanelsInfo() => _panelsInfo;

  String getManufacturer() => manufacturer;

  void changeManufacturer(var name) {
    manufacturer = name;
    notifyListeners();
  }

  List<Widget> _damagedPanels = [
    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
      Flexible(
          flex: 12,
          fit: FlexFit.tight,
          child: TextFormField(
            validator: (val) =>
                !val.contains('@') ? 'Invalid Panel Name' : null,
            onSaved: (val) {},
            style: GoogleFonts.lato(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
                textStyle: TextStyle(color: Color(0xFFF68D7F))),
            decoration: InputDecoration(
                labelText: 'Damaged Panel',
                contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0))),
          )),
      Flexible(
        flex: 2,
        fit: FlexFit.loose,
        child: Center(
          child: Container(
            padding: EdgeInsets.all(8.0),
            child: Icon(Icons.add_a_photo, color: Colors.grey.withOpacity(0.8)),
          ),
        ),
      )
    ])
  ];

  void addPanelField(Widget panel) {
    _panelsInfo.add(new Panel(name: null, image: null));
    notifyListeners();
    _damagedPanels.add(panel);

    notifyListeners();
  }

  void removePanelField() {
    if (_damagedPanels.length > 1) {
      _damagedPanels.removeLast();
    }
    if (_panelsInfo.length > 1) {
      _panelsInfo.removeLast();
    }
    notifyListeners();
  }

  void addPanelInfo() {
    _panelsInfo.add(new Panel(name: null, image: null));
    notifyListeners();
  }

  void removePanelInfo() {
    if (_panelsInfo.length > 1) {
      _panelsInfo.removeLast();
    }
    notifyListeners();
  }

  void addPanelImage(int index, File image) {
    _panelsInfo[index].image = image;
    notifyListeners();
  }
}
