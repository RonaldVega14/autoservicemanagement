import 'package:flutter/material.dart';

class PanelReference {
  PanelReference({@required this.downloadUrl, @required this.name});
  final String downloadUrl;
  final String name;

  factory PanelReference.fromMap(Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    final String downloadUrl = data['downloadUrl'];
    final String name = data['name'];
    if (downloadUrl == null) {
      return null;
    }
    return PanelReference(downloadUrl: downloadUrl, name: name);
  }

  Map<String, dynamic> toMap() {
    return {
      'downloadUrl': downloadUrl,
      'name': name,
    };
  }
}
