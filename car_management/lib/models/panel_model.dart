import 'dart:io';
import 'package:flutter/material.dart';

class Panel {
  String name;
  File image;

  Panel({@required this.name, @required this.image});

  factory Panel.fromMap(Map<String, dynamic> data) {
    if (data == null) {
      return null;
    }
    return Panel(name: data['name'] ?? '', image: data['image'] ?? '');
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'image': image,
    };
  }
}
