import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Manufacturer {
  String name;
  String logo;

  Manufacturer({@required this.name, @required this.logo});

  factory Manufacturer.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;

    return Manufacturer(
      name: data['name'] ?? "",
      logo: data['logo'] ?? "",
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'logo': logo,
    };
  }
}
