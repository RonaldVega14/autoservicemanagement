import 'package:car_management/models/manufacturer_model.dart';
import 'package:car_management/models/panel_reference.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Car {
  String id;
  Manufacturer manufacturer;
  String model;
  String color;
  String year;
  String provider;
  String vin;
  String recievedDate;
  List<PanelReference> damagedPanels;

  Car(
      {@required this.id,
      @required this.manufacturer,
      @required this.model,
      @required this.color,
      @required this.year,
      @required this.provider,
      @required this.vin,
      @required this.recievedDate,
      @required this.damagedPanels});

  factory Car.fromMap(Map<String, dynamic> data) {
    if (data == null) {
      return Car(
          id: "",
          manufacturer: Manufacturer(name: "", logo: ""),
          model: "",
          color: "",
          year: "",
          provider: "",
          vin: "",
          recievedDate: "",
          damagedPanels: []);
    }
    Manufacturer manufacturer =
        new Manufacturer(name: data['manufacturer'].name, logo: data['logo']);

    List<PanelReference> damagedPanels;
    data['damagedPanels'].forEach((panel) => damagedPanels.add(
        new PanelReference(
            downloadUrl: panel['downloadUrl'], name: panel['name'])));
    return Car(
        id: data['id'],
        manufacturer: manufacturer,
        model: data['model'] ?? "",
        color: data['color'] ?? "",
        year: data['year'] ?? "",
        provider: data['provider'] ?? "",
        vin: data['vin'] ?? "",
        recievedDate: data['recievedDate'] ?? "",
        damagedPanels: damagedPanels);
  }

  factory Car.fromFirestore(DocumentSnapshot doc) {
    Map data = doc.data;

    Manufacturer manufacturer = new Manufacturer(
        name: data['manufacturer']['name'], logo: data['manufacturer']['logo']);

    List<PanelReference> damagedPanels = [];

    for (var panel in data['damagedPanels']) {
      damagedPanels.add(new PanelReference(
          downloadUrl: panel['downloadUrl'], name: panel['name']));
    }

    return Car(
        id: data['id'],
        manufacturer: manufacturer ?? Manufacturer(name: "", logo: ""),
        model: data['model'] ?? "",
        color: data['color'] ?? "",
        year: data['year'] ?? "",
        provider: data['provider'] ?? "",
        vin: data['vin'] ?? "",
        recievedDate: data['recievedDate'] ?? "",
        damagedPanels: damagedPanels ?? []);
  }

  Map<String, dynamic> toMap() {
    return {
      'manufacturer': manufacturer,
      'model': model,
      'color': color,
      'year': year,
      'provider': provider,
      'vin': vin,
      'recievedDate': recievedDate,
      'damagedPanels': damagedPanels,
    };
  }
}
