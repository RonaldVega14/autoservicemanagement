import 'package:flutter/material.dart';
import 'package:car_management/pages/dashboard_page.dart';
import 'package:car_management/pages/signIn_page.dart';
import 'package:car_management/services/firebase_auth_service.dart';

class AuthWidget extends StatelessWidget {
  const AuthWidget({Key key, @required this.userSnapshot}) : super(key: key);
  final AsyncSnapshot<User> userSnapshot;
  @override
  Widget build(BuildContext context) {
    if (userSnapshot.connectionState == ConnectionState.active) {
      print('Connection State: $ConnectionState');
      return userSnapshot.hasData ? DashboardPage() : SignInPage();
    }
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
