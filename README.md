**Auto Service Management**
A Flutter application built from scratch using Provider for the state management.

Features:

- Calendar for schedule management. (one or many days selection)

- Minimal Profile UI.

- Profile Image picker. (Gallery or camera)

- ModalBottomSheet for the hour selection.

- Dynamic fields in forms.

- Push notifications.


**App Screenshots**
Only few screenshots because the full version of the app is confidential.


**Home Screen**

<img src="screenshots/HomeScreen.png" height = "400">

**Details Page**

<img src="screenshots/DetailsPage.png" height = "400">

**Form Page**

<img src="screenshots/FormPage.png" height = "400">

**List View Pages Design**

<img src="screenshots/ListViewDesign.png" height = "400">

**Profile Page**

<img src="screenshots/ProfilePage.png" height = "400">

**Calendar**

<img src="screenshots/Calendar.png" height = "400">
<img src="screenshots/Calendar1.png" height = "400">
<img src="screenshots/HourSelection.png" height = "400">



